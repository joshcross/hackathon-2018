var StandardBounties = artifacts.require("../contracts/StandardBounties.sol");
var Adoption = artifacts.require("Adoption");

module.exports = function(deployer) {
    deployer.deploy(StandardBounties, "0x0000000000000000000000000000000000000000");
    deployer.deploy(Adoption);
};
