App = {
  web3Provider: null,
  contracts: {
    artistName: "Some Artist",
    projectName: "New Album",
    projectDetails: "We need this to be good!",
    budget: 1000,
    contractType: "fixed"
  },
  globalProject: {init:true},

  init: function() {
    // Load pets.
    $.getJSON('../pets.json', function(data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i ++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);

        petsRow.append(petTemplate.html());
      }
    });
    App.bindEvents();
    return App.initWeb3();
  },

  initWeb3: function() {
    // Is there an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // If no injected web3 instance is detected, fall back to Ganache
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('StandardBounties.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var SBArtifact = data;
      App.contracts.StandardBounties = TruffleContract(SBArtifact);

      // Set the provider for our contract
      App.contracts.StandardBounties.setProvider(App.web3Provider);

      $.getJSON('Adoption.json', function(data) {
        // Get the necessary contract artifact file and instantiate it with truffle-contract
        var AdoptionArtifact = data;
        App.contracts.Adoption = TruffleContract(AdoptionArtifact);

        // Set the provider for our contract
        App.contracts.Adoption.setProvider(App.web3Provider);

        // Use our contract to retrieve and mark the adopted pets
        // return App.markAdopted();
        return App.getProjects();
      });
    });
  },

  bindEvents: function() {
    // $(document).on('click', '.btn-adopt', App.handleAdopt);
    // $(document).on('click', '#submit', App.createNewProjcet);
  },

  markAdopted: function(adopters, account) {
    var adoptionInstance;

    App.contracts.Adoption.deployed().then(function(instance) {
      adoptionInstance = instance;

      return adoptionInstance.getAdopters.call();
    }).then(function(adopters) {
      for (i = 0; i < adopters.length; i++) {
        if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
          $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
          console.log("Adopter hash: ", adopters[i]);
        }
      }
    }).catch(function(err) {
      console.log(err.message);
    });
  },

  handleAdopt: function(event) {
    console.log("Clicked!");
    event.preventDefault();

    var petId = parseInt($(event.target).data('id'));
console.log("Clicked ID:", petId);
    var adoptionInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];
      // console.log("Account: ", account);
      App.contracts.Adoption.deployed().then(function(instance) {
        adoptionInstance = instance;

        // Execute adopt as a transaction by sending account
        return adoptionInstance.adopt(petId, {from: account});
      }).then(function(result) {
        return App.markAdopted();
      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  createNewProject: function(event) {
    console.log("Create new project");
    var project = {
      artistName: $("#inputArtist").val(),
      projectName: $("#inputProject").val(),
      projectDetails: $("#projectDetails").val(),
      budget: $("#inputBudget").val(),
      contractType: $("#inputState").val()
    }
    App.globalProject = project;
    var now = new Date().getTime();
    // console.log("Artist Name: ", artistName);
    App.createBounty(now,JSON.stringify(project), project.budget, "0x0");
  },

  getProjects: function() {
    var projectInstance;
    console.log(App.globalProject);
    return App.globalProject;
    // App.contracts.StandardBounties.deployed().then(function(instance) {
    //   projectInstance = instance;
    //   // console.log(projectInstance);
    //   return projectInstance.getNumBounties.call();
    // }).then(function(numOfProjects) {
    //   console.log("Number of projects: ",numOfProjects);
    // }).catch(function(err) {
    //   console.log(err.message);
    // });
  },

  createBounty: (unixDeadline, requirements, fulfillment, arbiter) => {
      web3.eth.getAccounts(function(error, accounts) {
          if (error) {
              console.log(error);
          }

          var account = accounts[0];
          var instance;
          /// @dev issueBounty(): instantiates a new draft bounty
          /// @param _issuer the address of the intended issuer of the bounty
          /// @param _deadline the unix timestamp after which fulfillments will no longer be accepted
          /// @param _data the requirements of the bounty
          /// @param _fulfillmentAmount the amount of wei to be paid out for each successful fulfillment
          /// @param _arbiter the address of the arbiter who can mediate claims
          /// @param _paysTokens whether the bounty pays in tokens or in ETH
          /// @param _tokenContract the address of the contract if _paysTokens is
          App.contracts.StandardBounties.deployed().then((inst) => {
              instance = inst;
              //Requirements = String
              //Fulfilmment = uint256 of wei to pay

              console.log(account, unixDeadline, requirements, fulfillment, arbiter, false, "0x0");

              return instance.issueBounty(account, unixDeadline, requirements, fulfillment, arbiter, false, "0x0", {from: account});
              // return instance.issueAndActivateBounty(account, unixDeadline, requirements, fulfillment, arbiter, false, "0x0", fulfillment, {from: account});
          }).then((result) => {
              console.log("Bounty Created!");
              console.log(result);
              instance.activateBounty();
          }).catch(function(err) {
            console.log(err);
            console.log(err.message);
          });
      });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
