const HDWalletProvider = require('truffle-hdwallet-provider')
const fs = require('fs')

let secrets;
let mnemonic;
if (fs.existsSync('secrets.json')) {
    secrets = JSON.parse(fs.readFileSync('secrets.json', 'utf8'));
    mnemonic = secrets.mnemonic;
} else {
    console.log('No secrets.json found. If you are trying to publish EPM this will fail. Otherwise, you can ignore this message!');
    mnemonic = '';
}

module.exports = {
    networks: {
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*" // Match any network id
        }
    }
}
